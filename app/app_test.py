from unittest import TestCase
from unittest.mock import patch
from app import app, data
import json

class TestFlask(TestCase):

    def setUp(self):
        self.client = app.test_client()

    def test_coe332(self):
        # Test the / root route
        result = self.client.get('/')

        # The http status code should be OK
        self.assertEqual(result.status, "200 OK")

        # Decode the binary result returned by the test client into a utf-8 string
        data_string = result.data.decode("utf-8")
        data_dictionary = json.loads(data_string)

        # Assert that the returned structure has the data we want
        self.assertIn("instructors", data_dictionary)
        self.assertIn("meeting", data_dictionary)
        self.assertIn("assignments", data_dictionary)

    def test_instructors(self):
        # Test the /instructors route
        result = self.client.get('/instructors')
        self.assertEqual(result.status, "200 OK")
        data_string = result.data.decode("utf-8")
        data_dictionary = json.loads(data_string)
        self.assertEqual(len(data_dictionary), 3)

    def test_instructor_ari(self):
        # Test the /instructors/1 route
        result = self.client.get('/instructors/1')
        self.assertEqual(result.status, "200 OK")
        data_string = result.data.decode('utf-8')
        expected_string = '{"name": "Ari Kahn", "email": "akahn@tacc.utexas.edu"}'
        self.assertEqual(json.dumps(json.loads(data_string), sort_keys=True), json.dumps(json.loads(expected_string), sort_keys=True))

    def test_instructor_data(self):
        result = self.client.get('/instructors/1/email')
        self.assertEqual(result.status, "200 OK")
        data_string = result.data.decode('utf-8').strip()
        expected_string = '"akahn@tacc.utexas.edu"'
        self.assertEqual(data_string, expected_string)

    def test_instructor_nobody(self):
        # Test the /instructors/4 route
        result = self.client.get('/instructors/4')
        self.assertEqual(result.status, "404 NOT FOUND")

    def test_assignments(self):
        # Test the /assignments route
        result = self.client.get('/assignments')
        self.assertEqual(result.status, "200 OK")
        data_string = result.data.decode("utf-8")
        expected_string = '[{"name": "hw1", "url": "https://bitbucket.org/jchuahtacc/coe332-f2019-hw1", "points": 10}, {"name": "hw2", "url": "https://bitbucket.org/jchuahtacc/coe332-f2019-hw2", "points": 20}]'
        self.assertEqual(json.dumps(json.loads(data_string), sort_keys=True), json.dumps(json.loads(expected_string), sort_keys=True))

    def test_post_assignment(self):
        # Test that we can post a new assignment to /assignments
        # Testing json dictionary equivalence is weird, so we will make our best effort
        # Create a dictionary
        new_assignment = { 'name' : 'hw3', 'url' : 'https://bitbucket.org/jchuahtacc/coe332-f2019-hw3', 'points': 10 }

        # Post the string representation of the dictionary
        result = self.client.post('/assignments', data=json.dumps(new_assignment), content_type="application/json")
        self.assertEqual(result.status, "200 OK")
        result = self.client.get('/assignments')
        self.assertEqual(result.status, "200 OK")
        assignments = json.loads(result.data.decode("utf-8"))
        self.assertEqual(json.dumps(assignments[2], sort_keys=True), json.dumps(new_assignment, sort_keys=True))

    def test_meeting(self):
        # Test the /meeting route
        result = self.client.get('/meeting')
        self.assertEqual(result.status, "200 OK")
        data_string = result.data.decode("utf-8")
        expected_string = '{"days":["Tuesday","Thursday"],"end":1330,"location":"GDC 3.248","start":1100}'
        self.assertEqual(json.dumps(json.loads(data_string), sort_keys=True), json.dumps(json.loads(expected_string), sort_keys=True))

    def test_meeting_days(self):
        # Test the /meeting/days route
        result = self.client.get('/meeting/days')
        self.assertEqual(result.status, "200 OK")
        data_string = result.data.decode("utf-8")
        expected_string = '["Tuesday","Thursday"]'
        self.assertEqual(json.dumps(json.loads(data_string), sort_keys=True), json.dumps(json.loads(expected_string), sort_keys=True))

    def test_assignment_url(self):
        # Test the /assignments/0/url route
        result = self.client.get('/assignments/0/url')
        self.assertEqual(result.status, "200 OK")
        data_string = result.data.decode("utf-8").strip()
        expected_string = '"https://bitbucket.org/jchuahtacc/coe332-f2019-hw1"'
        self.assertEqual(data_string, expected_string)

    def test_out_of_bounds_404s(self):
        result = self.client.get('/instructors/1/doesntexist')
        self.assertEqual(result.status, "404 NOT FOUND")
        
        result = self.client.get('/meeting/doesntexist')
        self.assertEqual(result.status, "404 NOT FOUND")
        
        result = self.client.get('/assignments/5')
        self.assertEqual(result.status, "404 NOT FOUND")
        
        result = self.client.get('/assignments/1/doesntexist')
        self.assertEqual(result.status, "404 NOT FOUND")