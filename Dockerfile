FROM python:3-onbuild
#^Does COPY for you and does pip -R requirements

# tell the port number the container should expose
EXPOSE 5000

# run the command
CMD ["python", "./main.py"]
#CMD ["python", "./app.py"]
